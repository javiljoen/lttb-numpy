"""Steinarsson's LTTB algorithm (Numpy implementation)."""

from .lttb import downsample  # noqa
